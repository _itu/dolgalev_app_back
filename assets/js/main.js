var app;

(function ($) {

	var APP = function () {

		var self = this;

		self.vars = {
			body: $('body'),
			html: $('html'),
			header: $('#header'),
			main: $('#main'),
			footer: $('#footer'),
			doc: $(document),
			win: $(window),
			overlay: $('#overlay'),
			popup: $('#popup'),
			auth: $('#Authentificated'),
			notification: $('#Notification'),
		}

		if (self.vars.auth[0]) self.vars.auth_id = self.vars.auth.data('id')

		self.fn = {

			showNotification: function (type, text) {
				var message = text? text : (type == 'error')? 'Ошибка сервера' : 'Выполено успешно'
					, item = $('<div class="item '+type+'">'+message+'</div>')
				self.vars.notification.append(item)

				setTimeout(function () {
					item.fadeOut(function() {item.remove()})
				}, 5000);

			},


			requestHandler: function ($el, triggeredClass, req, addExtendParams, additionalFuncs) {

				// $el - jquery объект по которому произошел клик
				// triggeredClass - переключаемый класс на кнопке по которому определяется add/remove
				// req:
				//  req.entity - оперируемая сущность для "избранного"
				//  req.entityName - отображаемое имя сущности (для уведомлений)
				// addExtendParams - объект расширяющий стандартный объект параметров содержащий _csrf токен
				// additionalFuncs - дополнительная логика для обработки удачного завершения

				if ($el.hasClass(triggeredClass)) {
					var action = 'remove'
						, funcIndex = 0
						, notificationText = (req.type === 'favorites')
							? req.entityName + ' удален из избранного'
							: req.notification
				} else {
					var action = 'add'
						, funcIndex = 1
						, notificationText = (req.type === 'favorites')
							? req.entityName + ' добавлен в избранное'
							: req.notification
				}

				var reqParams = { '_csrf': self.vars.body.data('csrf') }
					, additionalFuncs = additionalFuncs || []
					, addExtendParams = addExtendParams || {}
					, url = ''

				switch(req.type){
					case 'favorites': 
						url = '/user/'+ self.vars.auth_id +'/favorites/'+req.entity+'/'+action+'/'+ $el.data(req.entity+'-id')
						break;

					case 'change':
						url = '/'+req.entity+'/'+ $el.data('id') +'/change'
						reqParams[req.changedField] = (action === 'add')? true : false
						break;
				}

				$.extend(reqParams, addExtendParams);
				
				$.post(url, reqParams)
				.done(function (data) {
					$el[action+'Class'](triggeredClass)
					if (additionalFuncs[funcIndex]) additionalFuncs[funcIndex]()
					self.fn.showNotification('info', notificationText)
				})
				.error(function() {
					self.fn.showNotification('error')
				});


			}


			// global functions

		}

		self._init();

	}



	APP.prototype._init = function() {

		for (var item in this) {
			if (item[0] !== '_' && typeof this[item] === 'function') this[item]();
		}

	};



	APP.prototype.higthpriority = function() {
		var self = this;

		// $('.a-activeItem').activeItem()
	};


	// ^^ HIGTH PRIORITY FUNCS ^^


	APP.prototype.selectize = function() {
		
		var self = this
			, items = $('.a-selectize')
			, temp
			, opts = {

				single: {
					valueField: 'id',
					labelField: 'name',
					searchField: 'name',
					create: false,
					onInitialize: function () {

						var self = this
							, selected = self.$input.data('selected')
							, ajax = self.$input.data('ajax')
							, options = self.$input.data('options')

						function set_selected () {
							if (selected) {
								
								if (isNaN(selected)) {
									selected.split(',').forEach(function (item) {
										self.addItem( isNaN(item)? item : item>>0 )
									})
								} else self.addItem( selected>>0 )
								
							}
						}

						if (options) {
							self.addOption(options)
							set_selected()
						} else if (ajax) {
							$.get(ajax)
							.done(function(data) {
								self.addOption(data)
								set_selected()
							});
						} 
					}
				},

				multi: {
					maxItems: 15
				}
				

			}

		$.extend(opts.multi , opts.single);

		if (!items.length) return;

		for (var i = 0; i < items.length; i++) {
			temp = items.eq(i)
			temp.removeClass('a-selectize').selectize( opts[ temp.data('selectize-type') ] )
		};

	};



	APP.prototype.datepicker = function(first_argument) {
		
		var self = this
			, items = $('.a-datepicker')
			, temp
			, opts = {

				default: {
					changeMonth: true,
					changeYear: true,
					dateFormat: 'yy-mm-dd',
					yearRange: '1920:2020'
				}

			}

		if (!items.length) return;
		$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );

		for (var i = 0; i < items.length; i++) {
			temp = items.eq(i)

			temp.removeClass('a-datepicker').datepicker( opts[ temp.data('datepicker-type') || 'default' ] )
		};
	};



	APP.prototype.ckeditor = function() {
		
		if ( !$('.a-ckeditor')[0] ) return;

		var self = this
			, items = $('.a-ckeditor')
			, opts = {
				maxi: {
					filebrowserImageUploadUrl: '/upload?ckeditor=true&_csrf='+self.vars.csrf,
					extraPlugins: 'image2,divarea', 
					removeButtons: 'Anchor',
					removePlugins: 'stylescombo,specialchar',
					format_tags: 'p;h2;h3;h4;address',
					toolbarGroups: [
						{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
						{ name: 'styles' },
						{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
						{ name: 'links' },
						{ name: 'insert' },
						{ name: 'tools' },
						// { name: 'document',    groups: [ 'mode' ] }
					]
				}, 
				mini: {
					filebrowserImageUploadUrl: '/upload?ckeditor=true',
					extraPlugins: 'image2,divarea',
					removeButtons: 'Anchor',
					removePlugins: 'stylescombo,specialchar',
					format_tags: 'p;h2;h3;h4;address',
					toolbarGroups: [
						{ name: 'insert' },
						{ name: 'tools' },

					]
				},
				event: {
					filebrowserImageUploadUrl: '/upload?ckeditor=true',
					extraPlugins: 'image2,divarea', 
					removeButtons: 'Anchor',
					removePlugins: 'stylescombo,specialchar',
					format_tags: 'p;h4',
					toolbarGroups: [
						{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
						{ name: 'styles' },
						{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
						{ name: 'links' },
						{ name: 'insert' },
						{ name: 'tools' },
					]
				},
			}



	};




	// ---- HIGHT PRIORITY FUNCS ----

		APP.prototype.searchGlobalHandler = function() {
		
		var self = this,
			el = $('#search-global'),
			button = el.find('.button'),
			input = el.find('.search-global_input'),
			isClosed = true;


		el.on('click', function(event) {
			event.preventDefault();
			event.stopPropagation();
		});


		input.on('focus', function(event) {
			self.vars.doc.on('keypress.searchEnable', function(event) {
				if (event.which == 13) button.trigger('click')
			});
		});


		input.on('blur', function(event) {
			self.vars.doc.off('keypress.searchEnable');
		});


		button.on('click', function(event) {
			
			if (isClosed) {

				TweenMax.to(input, .3, {css: {width: "150px"}})
				input.focus();
				isClosed = false;

				self.vars.body.on('click.searchClose', function(event) {
					
					TweenMax.to(input, .3, {css: {width: "0px"}})
					isClosed = true;
					self.vars.body.off('click.searchClose')

				});

			} else {
				window.location.href = '/search?entity=post&text=' + input.val()
			}
			
		});

	};


	// Оверлэй и вспывающие окна

	APP.prototype.editEndOfWord = function() {

		var self = this
		
		$('.a-eeow').each(function(index, el) {
			var $el = $(el)
				, opt = $el.data('eeow').split(',')

			$el.html( wordPostfix(opt[0], opt[1], opt[2], opt[3]) )
		});

		function wordPostfix(num, word, word1, word2) {
 
			var number = (typeof num == 'number')? String(num) : num,
					result = word;
		 
			if (number.slice(-2, -1) != "1") {
				if (number.slice(-1) == "2" ||
						number.slice(-1) == "3" ||
						number.slice(-1) == "4") result = word2;
				else if (number.slice(-1) == "1") result = word1;
			}
		
			return number +' '+ result;
		}

	};



	APP.prototype.overlay = function() {
		
		var self = this
			, blocks = {
					upload: $('.b-popup-upload'),
					uploadInGallery: $('.b-popup-uploadInGallery')
				}
			, buttons = {
					openUploadInGallery: $('.a-popup-uploadInGallery')
				}
			, popup = self.vars.popup
			, upload_target_field
			, lastY = 0
			, edit_page = $('#page_edit')
			, headerHeigth = self.vars.header.innerHeight()
			, accuracy = 50
			, acc_hH = headerHeigth + accuracy


		function openPopup (data) {
			self.vars.popup.empty().append(data);
			self.vars.html.css('overflow', 'hidden');
			self.vars.overlay.show();
			self.vars.win.trigger('scroll')
			// self.vars.popup.css('margin-left', -self.vars.popup.innerWidth()/2);
			// TweenMax.from(self.vars.popup, .5, {css: {autoAlpha: 0, y: -100}, ease:Back.easeOut})
			setTimeout(function(){self.vars.win.trigger('scroll')}, 10);
			
		}

		function closePopup () {
			self.vars.overlay.fadeOut('200');
			self.vars.html.css('overflow', 'auto');
		}

		self.vars.overlay.on('click', function(event) {
			event.stopPropagation();
			closePopup()
		});

		self.vars.popup.on('click', function(event) {
			event.stopPropagation();
		});

	};



	APP.prototype.authentificated = function() {
		
		var self = this
			, root = $('#Authentificated')
			, buttonMenu = root.find('.button')
			, menu = root.find('.auth_menu')

		if ( !root[0] ) return;

		buttonMenu.on('click', openMenu )
		

		function openMenu (event) {
			event.stopPropagation()
			menu.show()
			buttonMenu.off('click'); 
			buttonMenu.on('click', closeMenu )
			self.vars.body.on('click.auth_menu', closeMenu);
		}

		function closeMenu (event) {
			menu.hide()
			buttonMenu.off('click');
			self.vars.body.off('click.auth_menu');
			buttonMenu.on('click', openMenu )
		}

	};



	APP.prototype.notification = function() {

		var self = this
		
		$('#Notification').on('click', '.item', function(event) {
			$(this).off('click').fadeOut(function () {
				$(this).remove()
			});
		});

	};












	// APP.prototype.edit = function() {

	//   if (!$('#page_edit')[0]) return;

	//   var self = this
	//     , page = $('#page_edit')
	//     , hideTimer
	//     , patterns = page.find('.a-pattern')
	//     , buttonDelete = $('<div style="z-index: 100; display: none; right: -16px" class="a-deleteAdditionalFiel" title="Удалить поле">&#8211;</div>')
		
	//   patterns.find('[required]').each(function(index, el) {
	//     var $el = $(el)
	//     $el.attr('required', null)
	//     $el.attr('data-required', 'true')
	//   });

	//   function show_delete_button (event) {
	//     event.stopPropagation();
	//     var $this = $(this)
	//     clearTimeout(hideTimer)
	//     buttonDelete.css('top', $this.position().top+17+'px' ).off('click').on('click', function(event) {
	//       $this.remove()
	//       buttonDelete.hide()
	//     });

	//     if (this.tagName.toLowerCase() === 'input') $this.closest('.col2').append(buttonDelete)
	//     else $this.append(buttonDelete)

	//     $this.on('mouseleave', hide_delete_button)
	//     buttonDelete.show()
	//   }


	//   function hide_delete_button (event) {
	//     event.stopPropagation();
	//     $(this).off('mouseleave')
	//     hideTimer = setTimeout(function(){buttonDelete.hide()}, 2000);
	//   }


	//   page.on('change', '.a-pseudoField', function(event) {
	//     var connector = $(this).data('connector')
	//       , aggregatedData = $('.a-pseudoField').filter('[data-connector='+connector+']').map(function (i, item) {return $(item).val()}).toArray().join('=')
	//     console.log(connector, aggregatedData);
	//     $('#'+connector).val(aggregatedData)
	//   });

	//   page.on('mouseenter', '.a-canBeDeleted', show_delete_button);

	//   page.on('click', '.a-createAdditionalField', function(event) {
			
	//     var $this = $(this)
	//       , type = $this.data('field-type')

	//     switch(type){
	//       case 'simple':
	//         var original = $this.children('.a-pattern')
	//           , parent = $this.closest('.fieldSet')
	//           , clone = original.clone().val('').toggleClass('a-pattern a-canBeDeleted').attr('name', original.data('name'));

	//         parent.append(clone)
	//         break;


	//       case 'pseudo':
	//         var original = $this.children('.a-pattern')
	//           , now_sets = $this.siblings('.itemSet')
	//           , connector = now_sets.length? now_sets.last().children()[0].id.replace(/\d+$/, function (num) {return (num>>0)+1}) : original.children()[0].id
	//           , clone = original.clone().toggleClass('a-pattern a-canBeDeleted')

	//         clone.find('[data-name]').each(function(index, el) {
	//           var $el = $(el)
	//           $el.attr('name', $el.data('name'));
	//         });

	//         clone.find('.a-pseudoField').each(function (i, item) {
	//           var $item = $(item)
	//           if (item.tagName.toLowerCase() === 'select') $item.addClass('a-selectize').find('option').remove()
	//           $item.attr('data-connector', connector).val('')
	//         })

	//         clone.find('input[type=hidden]').attr({
	//           'id': connector,
	//           'value': '',
	//           'name': connector.replace(/(:?for_)(\D+)(_\d+)$/, function (str, s0, s1, s2) {return s1+'['+s2.replace('_','')+']'})
	//         })

	//         $this.parent().append(clone)
	//         self.datepicker()
	//         self.selectize()
	//         break;


	//       case 'itemset':
	//         var original = $this.children('.a-pattern')
	//           , setname = original.data('setname')
	//           , regexp = new RegExp(setname+'\\D*(\\d+)')
	//           , now_sets = $this.siblings('.itemSet')
	//           , counter = now_sets.length? (now_sets.last().find('[name]').attr('name').match(regexp)[1] >> 0)+1 : 0
	//           , clone = original.clone().toggleClass('a-pattern a-canBeDeleted')
	//           , nested_pattern = clone.find('.a-pattern')
	//           , temp_place
	//           , temp

	//         if (nested_pattern.length) {
	//           temp = nested_pattern.first()
	//           temp_place = temp.parent()
	//           temp.remove()
	//         }

	//         function change_attr ($el, attr_name, source) {
	//           $el.attr(attr_name, source.replace(regexp, function (str, s1) {
	//             return str.slice(0, -(s1.length) ) + counter
	//           }) ).val('')
	//         }

	//         clone.find('[data-name]').each(function (i, item) {
	//           var $item = $(item)
	//             , name =  $item.data('name')
	//           if (name==='id') return $item.remove()
	//           if (item.tagName.toLowerCase() === 'select') $item.addClass('a-selectize').find('option').remove()
	//           else if (item.tagName.toLowerCase() === 'textarea' && $item.data('ck-type')) $item.addClass('a-ckeditor')
	//           else if ($item.hasClass('hasDatepicker')) $item.toggleClass('hasDatepicker a-datepicker').removeAttr('id')
	//           if ( $item.data('required') ) $item.attr('required', 'true')
	//           change_attr($item, 'name', name)
	//         })

	//         clone.find('.a-popup-upload').each(function (i, item) {
	//           var $item = $(item)
	//           $item.attr('data-target', $item.prev('input').attr('name'))
	//         })

	//         if (temp) {
	//           temp.find('[data-name]').each(function (i, item) {
	//             var $item = $(item)
	//               , name =  $item.data('name')
	//             if (name==='id') return $item.remove()
	//             change_attr($item, 'data-name', name)
	//           })
	//           temp_place.append(temp)
	//         };

	//         $this.parent().append(clone)
	//         self.selectize()
	//         self.datepicker()
	//         self.ckeditor()
	//         break;

	//     }
	//   });

	// };







	APP.prototype.lowpriority = function() {

		var self = this

		self.vars.overlay.css('height', self.vars.doc.innerHeight())
	};





	// Инициализация приложения
	app = new APP;

})(jQuery)


