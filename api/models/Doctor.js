var Validate = require('../services/Validate');

/**
* Doctor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    is_image:  Validate.is_image,
    is_schedule: Validate.is_schedule

  },

  attributes: {

    last_name: {
      type: 'string',
      required: true
    },

    first_name: {
      type: 'string',
      required: true
    },

    second_name: {
      type: 'string',
      required: true
    },

    email: {
      type: 'string',
      email: true
    },
    
    photo: {
      type: 'string',
      is_image: true
    },

    is_working: {
      type: 'boolean',
      defaultsTo: true
    },

    schedule: {
      type: 'json',
      is_schedule: true
    },


    patients: {
      collection: 'user',
      via: 'doctor_id'
    },


    getFullname: function () {
      return this.last_name+' '+this.first_name+(this.second_name? ' '+this.second_name : '')
    },


    toJSON: function() {
      var obj = this.toObject();
      delete obj.createdAt
      delete obj.is_working
      delete obj.is_visited
      delete obj.is_rejected
      return obj
    }

  },

  afterCreate: function (vals, next) {
    var tmp = (_.isArray(vals))? vals[0] : vals
    sails.sockets.blast('update', 'doctors')
    User.update({},{mod_doctors: tmp.createdAt}).exec(next)
  },

  afterUpdate: function (vals, next) {
    var tmp = (_.isArray(vals))? vals[0] : vals
    sails.sockets.blast('update', 'doctors')
    User.update({},{mod_doctors: tmp.updatedAt}).exec(next)
  }
};

