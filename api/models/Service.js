var Data = require('../services/Data');
/**
* Service.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    name: {
      type: 'string',
      required: true
    },

    price: {
      type: 'number',
      required: true
    },

    type: {
      type: 'string',
      required: true,
      enum: Data.types.service
    },

    description: 'text'

  }
};

