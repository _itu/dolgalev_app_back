/**
* Reception.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {

		datetime: {
			type: 'datetime',
			required: true
		},

		is_rejected: {
			type: 'boolean',
			defaultsTo: false
		},

		is_visited: {
			type: 'boolean',
			defaultsTo: false
		},

		provided_services: 'json',

		final_price: 'integer',

		user_id: {
			model: 'user',
			required: true
		},

		doctor_id: {
			model: 'doctor',
			required: true
		},

		comments: {
			collection: 'comment',
			via: 'reception_id'
		},

		toJSON: function() {
			var obj = this.toObject();
			delete obj.createdAt
			delete obj.user_id
			delete obj.is_visited
			delete obj.is_rejected
			return obj
		}


	},


	afterCreate: function (vals, next) {
		var tmp = (_.isArray(vals))? vals[0] : vals
		sails.sockets.blast('update', 'receptions')
		User.update(
			{id: tmp.user_id}, 
			{mod_receptions: tmp.createdAt}
		).exec(next)
	},

	afterUpdate: function (vals, next) {
		var tmp = (_.isArray(vals))? vals[0] : vals
		sails.sockets.blast('update', 'receptions')
		User.update(
			{id: tmp.user_id}, 
			{mod_receptions: tmp.updatedAt}
		).exec(next)
	}
};

