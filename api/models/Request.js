var Validate = require('../services/Validate');
var Data = require('../services/Data');

/**
* Request.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  types: {

    is_time: Validate.is_time

  },

  attributes: {

    date: {
      type: 'date',
      after: function() {return new Date()},
      required: true
    },

    time_begin: {
      type: 'string',
      is_time: true
    },

    time_end: {
      type: 'string',
      is_time: true
    },

    state: {
      type: 'string',
      enum: Data.states.request,
      defaultsTo: Data.states.request[0]
    },



    user_id: {
      model: 'user',
      required: true
    },

    doctor_id: {
      model: 'doctor'
    },

    comment_id: {
      model: 'comment'
    },


    toJSON: function() {
      var obj = this.toObject();
      delete obj.createdAt
      delete obj.user_id
      delete obj.state
      return obj
    }

  },


  afterCreate: function (vals, next) {
    var tmp = (_.isArray(vals))? vals[0] : vals
    sails.sockets.blast('update', 'requests')
    User.update(
      {id: tmp.user_id}, 
      {mod_requests: tmp.createdAt}
    ).exec(next)
  },

  afterUpdate: function (vals, next) {
    var tmp = (_.isArray(vals))? vals[0] : vals
    sails.sockets.blast('update', 'requests')
    User.update(
      {id: tmp.user_id}, 
      {mod_requests: tmp.updatedAt}
    ).exec(next)
  }
};

