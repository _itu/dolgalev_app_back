/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

  attributes: require('waterlock').models.user.attributes({
    
    last_name: {
      type: 'string',
      required: true
    },

    first_name: {
      type: 'string',
      required: true
    },

    second_name: 'string',

    phone: 'string',

    photo: 'string',

    mod_doctors: 'datetime',
    mod_requests: 'datetime',
    mod_receptions: 'datetime',

    role: {
      type: 'string',
      enum: ['client', 'admin', 'moderator'],
      defaultsTo: 'client'
    },

    requests: {
      collection: 'request',
      via: 'user_id'
    },

    receptions: {
      collection: 'reception',
      via: 'user_id'
    },

    doctor_id: {
      model: 'doctor'
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.updatedAt
      return obj
    },

    getFullname: function () {
      return this.last_name+' '+this.first_name+(this.second_name? ' '+this.second_name : '')
    }
    
  }),

  // TODO: поправить в waterlock чтобы на create/update удалялся паремент `role`
  
  beforeCreate: function (vals, next) {
    if (vals.role !== 'client') delete vals.role
    next()
  },

  beforeUpdate: function (vals, next) {
    delete vals.role
    next()
  }

  // beforeCreate: require('waterlock').models.user.beforeCreate,
  // beforeUpdate: require('waterlock').models.user.beforeUpdate
};
