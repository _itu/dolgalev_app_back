module.exports = function(req, res, next) {
  waterlock.validator.validateTokenRequest(req, function(err, user){
    if(err) return res.forbidden(err)

    next();
  });
};
