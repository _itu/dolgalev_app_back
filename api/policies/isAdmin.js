module.exports = function(req, res, next) {
  if (!req.session.authenticated || req.session.user.role !== 'admin' ) {
    req.flash('error', 'Войдите как администратор')
    return res.redirect('/auth/login')
  }
  
  next()
};