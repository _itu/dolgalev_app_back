module.exports = function(req, res, next) {
  if (!req.session.authenticated){
    if (req.wantsJSON) 
      return res.json({errorType: 1})
    else 
      return res.forbidden('Access denied')
  }
    
  
  next()
};
