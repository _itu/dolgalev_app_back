/**
 * UserController.js 
 * 
 * @module      :: Controller
 * @description :: Provides the base user
 *                 actions used to make waterlock work.
 *                 
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = require('waterlock').actions.user({
	
	list: function (req, res, next) {
		if (req.wantsJSON) {
			User.query('SELECT id, first_name, second_name, last_name FROM user WHERE role="client"', function (err, users) {
				if ( Er.default(req, res, err, 500) ) return

				users = users.map(function (user) {
					return {id: user.id, name: user.last_name+' '+user.first_name+(user.second_name? ' '+user.second_name : '')}
				})

				res.json(users);
			})
		}else{
			User.find({
				role: 'client'
			}).exec(function (err, users) {
				if ( Er.default(req, res, err, 500) ) return
				
				res.view('user', { users: users} );
			})
		}
		
	},
	

	item: function (req, res, next) {
		User.findOneById( req.param('id') )
		.populate('doctor_id').populate('auth')
		.exec(function (err, user) {
			if ( Er.default(req, res, err, 500) ) return
			
			if (!user) {
				sails.log.warn('user '+req.param('id')+' not found')
				return res.notFound()
			};

			res.view('user/item', { user: user } );
		})
	},

});