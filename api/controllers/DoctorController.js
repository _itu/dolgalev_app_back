/**
 * DoctorController
 *
 * @description :: Server-side logic for managing doctors
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'list': function (req, res, next) {
    if (req.wantsJSON) {
      Doctor.query('SELECT id, first_name, second_name, last_name FROM doctor WHERE is_working=true', function (err, doctors) {
        if ( Er.default(req, res, err, 500) ) return
        
        doctors = doctors.map(function (doctor) {
          return {id: doctor.id, name: doctor.last_name+' '+doctor.first_name+(doctor.second_name? ' '+doctor.second_name : '')}
        })

        res.json(doctors);
      })
    }else{
      Doctor.find().exec(function (err, doctors) {
        if ( Er.default(req, res, err, 500) ) return
        
        res.view('doctor', { doctors: doctors} );
      })
    }

    
  },
  

  'item': function (req, res, next) {
    Doctor.findOneById( req.param('id') ).exec(function (err, doctor) {
      if ( Er.default(req, res, err, 500) ) return
      
      if (!doctor) {
        sails.log.warn('doctor '+req.param('id')+' not found')
        return res.notFound()
      };

      res.view('doctor/item', { doctor: doctor } );
    })
  },

  'new': function (req, res, next) {
    res.view('doctor/editOrCreate', {action: 'create', doctor: {}} )
  },


  'edit': function (req, res, next) {
    Doctor.findOneById(req.param('id')).exec(function (err, doctor) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('doctor/editOrCreate', {action: 'edit', doctor: doctor} )
    })
  },


  'create': function (req, res, next) {
    Doctor.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/doctor/' + created.id)
    })
  },

  'update': function (req, res, next) {
    Doctor.update( {id: req.param('id')}, req.params.all() ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/doctor/' + req.param('id'))
    })
  },

  'delete': function (req, res, next) {
    Doctor.destroy( req.param('id') ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/doctor')
    })
  }

};

