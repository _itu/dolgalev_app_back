/**
 * AuthController
 *
 * @module      :: Controller
 * @description	:: Provides the base authentication
 *                 actions used to make waterlock work.
 *
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = require('waterlock').waterlocked({

  jwt: function (req, res, next) {
    if (req.wantsJSON) 
      res.ok('logined with JSON web token')
    else {
      req.flash('info', 'Вы вошли при помощи токена')
      res.redirect('back')
    }
  },

});