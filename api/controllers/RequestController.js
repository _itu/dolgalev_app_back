/**
 * RequestController
 *
 * @description :: Server-side logic for managing requests
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  'list': function (req, res, next) {
    Request.find({
      state: 'processing',
      date: {'>': new Date()}
    }).populate('user_id').exec(function (err, requests) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('request', { requests: requests} );
    })
  },
  

  'item': function (req, res, next) {
    Request.findOneById( req.param('id') )
    .populate('user_id').populate('doctor_id').exec(function (err, request) {
      if ( Er.default(req, res, err, 500) ) return
      
      if (!request) {
        sails.log.warn('request '+req.param('id')+' not found')
        return res.notFound()
      };

      res.view('request/item', { request: request } );
    })
  },

  'new': function (req, res, next) {
    res.view('request/editOrCreate', {action: 'create', request: {}} )
  },


  'edit': function (req, res, next) {
    Request.findOneById(req.param('id'))
    .exec(function (err, request) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('request/editOrCreate', {action: 'edit', request: request} )
    })
  },


  'create': function (req, res, next) {
    var params = req.params.all()
    params.user_id = req.session.user.id

    Request.create( params ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return

      if (req.wantsJSON) 
        res.json(created)
      else
        res.redirect('/request/' + created.id)
    })
  },

  'update': function (req, res, next) {
    Request.update( {id: req.param('id')}, req.params.all() ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/request/' + req.param('id'))
    })
  },

  'delete': function (req, res, next) {
    Request.destroy( req.param('id') ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/request')
    })
  },


  'reject': function (req, res, next) {
    Request.update( {id: req.param('id')}, {state: 'rejected'} ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.ok()
    })
  },



};

