module.exports = {
	
  check_updates: function (req, res, next) {
    var user_id = req.session.user.id
      , needsUpdate = {}
      , fnArr = [

        function (cb) {
          if (!needsUpdate.receptions) return cb(null, null);
          Reception.find({
            user_id: user_id,
            datetime: { '>': new Date() }
          }).exec(cb)
        },

        function (cb) {
          if (!needsUpdate.requests) return cb(null, null);
          Request.find({
            user_id: user_id,
            date: {'>': new Date()},
            state: 'processing'
          }).exec(cb)
        },

        function (cb) {
          if (!needsUpdate.doctors) return cb(null, null);
          Doctor.find({
            is_working: true
          }).exec(cb)
        }
      ]


    User.findOneById(user_id).exec(function (err, user) {
      if ( Er.default(req, res, err, 500) ) return

      var u_res = {}

      function modComparser (req) {
        ['doctors', 'receptions', 'requests'].forEach(function (item) {
          var nowDate = new Date(user['mod_'+item])
            , incDate = new Date(req.param(item))
            , diff = Math.abs( nowDate.getTime() - incDate.getTime() )
          if ( isNaN(diff) || diff > 2000 ) {
            needsUpdate[item] = true
            user['mod_'+item] = new Date()
            u_res['mod_'+item] = user['mod_'+item]
          }
        })
      }

      modComparser(req)

      user.save(function (err) {
        if (err) sails.log('user save param after sync FAILED!')
      })
      async.parallel(fnArr, function (err, result) {
        if ( Er.default(req, res, err, 500) ) return
        
        result[0] && (u_res.receptions = result[0])
        result[1] && (u_res.requests = result[1])
        result[2] && (u_res.doctors = result[2])

        res.json(u_res)
      })

    })

  },


  reports: function (req, res, next) {

    var opts = {
      price: {'>' : 0},
      user_id: req.session.user.id
    }

    req.param('periodBegin') && (opts.datetime = {'>': new Date( req.param('periodBegin')) })
    req.param('periodEnd') && opts.datetime
      ? (opts.datetime['<'] = new Date(req.param('periodEnd')) )
      : (opts.datetime = {'<': new Date( req.param('periodEnd')) })

    Receptions.find(opts).exec(function (err, receptions) {
      if ( Er.default(req, res, err, 500) ) return

      res.json(receptions)
    })
  },

  test: function (req, res, next) {
    res.json(req.session)
  },

};

