/**
 * ServiceController
 *
 * @description :: Server-side logic for managing services
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'list': function (req, res, next) {

    var opts = {}

    if (req.param('type')) opts.type = req.param('type')
    if (req.param('text')) opts.name = {'contains': req.param('text')}

    Service.find(opts).exec(function (err, services) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('service', { services: services} );
    })
  },
  

  'item': function (req, res, next) {
    Service.findOneById( req.param('id') ).exec(function (err, service) {
      if ( Er.default(req, res, err, 500) ) return
      
      if (!service) {
        sails.log.warn('service '+req.param('id')+' not found')
        return res.notFound()
      };

      res.view('service/item', { service: service } );
    })
  },

  'new': function (req, res, next) {
    res.view('service/editOrCreate', {action: 'create', service: {}} )
  },


  'edit': function (req, res, next) {
    Service.findOneById(req.param('id')).exec(function (err, service) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('service/editOrCreate', {action: 'edit', service: service} )
    })
  },


  'create': function (req, res, next) {
    Service.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/service/' + created.id)
    })
  },

  'update': function (req, res, next) {
    Service.update( {id: req.param('id')}, req.params.all() ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/service/' + req.param('id'))
    })
  },

  'delete': function (req, res, next) {
    Service.destroy( req.param('id') ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/service')
    })
  }

};

