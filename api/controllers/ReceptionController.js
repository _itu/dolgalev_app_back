/**
 * ReceptionController
 *
 * @description :: Server-side logic for managing receptions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'list': function (req, res, next) {

    var opts = {
      is_rejected: false,
      is_visited: false
    }


    Reception.find(opts).populate('user_id').exec(function (err, receptions) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('reception', { receptions: receptions} );
    })
  },
  

  'item': function (req, res, next) {
    Reception.findOneById( req.param('id') )
    .populate('user_id').populate('doctor_id')
    .exec(function (err, reception) {
      if ( Er.default(req, res, err, 500) ) return
      
      if (!reception) {
        sails.log.warn('reception '+req.param('id')+' not found')
        return res.notFound()
      };

      res.view('reception/item', { reception: reception } );
    })
  },

  'new': function (req, res, next) {
    res.view('reception/editOrCreate', {action: 'create', reception: {}} )
  },


  'edit': function (req, res, next) {
    Reception.findOneById(req.param('id'))
    .exec(function (err, reception) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('reception/editOrCreate', {action: 'edit', reception: reception} )
    })
  },


  'create': function (req, res, next) {
    var params = req.params.all()
    Reception.create( params ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      
      if (params.from_request) {
        Request.update({ id: params.from_request }, { state: 'done' }, function (err) {
          if (err) sails.log(err);
        })
      }

      res.redirect('/reception/' + created.id)
    })
  },

  'update': function (req, res, next) {
    Reception.update( {id: req.param('id')}, req.params.all() ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/reception/' + req.param('id'))
    })
  },

  'delete': function (req, res, next) {
    Reception.destroy( req.param('id') ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/reception')
    })
  }

};

