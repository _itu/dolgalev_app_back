/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  'list': function (req, res, next) {

    var opts = {}

    if (req.param('type')) opts.type = req.param('type')
    if (req.param('text')) opts.name = {'contains': req.param('text')}

    Comment.find(opts).exec(function (err, comments) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('comment', { comments: comments} );
    })
  },
  

  'item': function (req, res, next) {
    Comment.findOne( req.param('id') ).exec(function (err, comment) {
      if ( Er.default(req, res, err, 500) ) return
      
      if (!comment) {
        sails.log.warn('comment '+req.param('id')+' not found')
        return res.notFound()
      };

      res.view('comment/item', { comment: comment } );
    })
  },

  'new': function (req, res, next) {
    res.view('comment/editOrCreate', {action: 'create', comment: {}} )
  },


  'edit': function (req, res, next) {
    Comment.findOneById(req.param('id'))
    .exec(function (err, comment) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.view('comment/editOrCreate', {action: 'edit', comment: comment} )
    })
  },


  'create': function (req, res, next) {
    Comment.create( req.params.all() ).exec(function (err, created) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/comment/' + created.id)
    })
  },

  'update': function (req, res, next) {
    Comment.update( {id: req.param('id')}, req.params.all() ).exec(function (err, updated) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/comment/' + req.param('id'))
    })
  },

  'delete': function (req, res, next) {
    Comment.destroy( req.param('id') ).exec(function (err) {
      if ( Er.default(req, res, err, 500) ) return
      
      res.redirect('/comment')
    })
  }

};

