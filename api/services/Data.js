module.exports = {

  month_list: ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],

  types: {
    service: ['implantology', 'surgery', 'stomatology']
  },

  states: {
    request: ['processing', 'done', 'rejected']
  }

}