module.exports = {

  no_script: function (text) {
    return text.search(/<\s*script/i) === -1? true : false
  },

  is_image: function (text) {
    return text.search(/((?:[^\/]+.)+\.(?:jpg|png|jpeg))$/) === -1? false : true
  },

  is_schedule: function (arr) {
    arr = (typeof arr === 'string')? JSON.parse(arr) : arr
    if (!_.isArray(arr) 
      || (arr.length !== 7)
      || !_.isObject(arr[0]) 
      ) return false
    else 
      return true
  },

  is_time: function (str) {
    var tmp = str.split(':')
    if (tmp.length !== 2 
      || (isNaN(tmp[0]) || tmp[0] > 24)
      || (isNaN(tmp[1]) || tmp[1] > 60)
      ) return false
    else
      return true
  }

}