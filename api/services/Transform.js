module.exports = {

	num_to_word_month: function (num) {
		return Data.month_list[num]
	},


	date_to_string: function (date, bool) {
		return date.getDate() +' '+ Transform.num_to_word_month( date.getMonth() ) + (bool? ' '+ date.getFullYear() : '')
	},

	date_to_range: function (date, duration) {
		if (duration == 1) return Transform.date_to_string(date, true);
		return Transform.date_to_string(date) +' - '+ Transform.date_to_string( new Date( date.getTime() + (86400000*( (duration-1) >> 0))), true)
	},

	time_to_string: function (date) {
		var time = new Date(date)
			, hour = time.getHours()+''
			, min = time.getMinutes()+''
		
		return (hour.length===2? hour : '0'+hour) +':'+ (min.length===2? min : '0'+min)
	},


	datetime_to_date_value: function (datetime) {
		var month = datetime.getMonth()+1+'' 
			, day = datetime.getDate()+'' 
		return ''+ datetime.getFullYear()+'-'+(month.length===2? month: '0'+month)+'-'+(day.length===2? day: '0'+day)
	},

	datetime_to_time_value: function (datetime) {
		var hours = datetime.getHours()+'' 
			, minutes = datetime.getMinutes()+'' 
		return (hours.length===2? hours: '0'+hours)+':'+(minutes.length===2? minutes: '0'+minutes)
	},

	datetime_to_local: function (datetime) {
		var tmp = (datetime instanceof Date)? datetime : new Date(datetime)
		tmp = tmp.toJSON()
		return tmp.slice(0, tmp.length - 8)
	},

	datetime_to_utc4: function (datetime) {
		var z = (datetime instanceof Date)? datetime : new Date(datetime)
		z.setTime(z.getTime() + 14400000)
		return z
	},

	time_value_to_datetime: function (string) {
		return new Date('0-01-01 '+string)
	},


	edit_end_of_word: function (num, str, locale) {
			
		var number = (typeof num == 'number')? String(num) : num
			, result = sails.__({phrase: str+'_5', locale: locale});
	 
		if (number.slice(-2, -1) != "1") {
			if (number.slice(-1) == "2" ||
					number.slice(-1) == "3" ||
					number.slice(-1) == "4") result = sails.__({phrase: str+'_2', locale: locale});
			else if (number.slice(-1) == "1") result = sails.__({phrase: str+'_1', locale: locale});
		}
	
		return result;
	}

}