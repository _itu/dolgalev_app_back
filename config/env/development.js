module.exports = {

  domain: 'localhost',
  protocol: 'http',
  port: 1337,

  models: {
    connection: 'devMySql'
  }

};
