module.exports = {

  domain: 'app.implantat-sk.ru',
  protocol: 'http',
  port: 11000,

  models: {
    connection: 'mySql'
  },


  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  // log: {
  //   level: "silent"
  // }

};
