module.exports.routes = {


  '/': 'request.list',


  'GET /api/check_updates': {
    controller: 'Api',
    action: 'check_updates',
    cors: true
  },
  'GET /api/reports': {
    controller: 'Api',
    action: 'reports',
    cors: true
  },
  'GET /api/test': {
    controller: 'Api',
    action: 'test',
    cors: true
  },


  'POST /auth/create': 'auth.create',
  'GET /auth/login': {view: 'auth/form'},
  'POST /auth/login': 'auth.login',
  'POST /auth/jwt': 'auth.jwt',
  'GET /auth/logout': 'auth.logout',
  '/auth/reset': 'auth.reset',


  'GET /request': 'request.list',
  'POST /request': {
    controller: 'Request',
    action: 'create',
    cors: true
  },
  'GET /request/:id': 'request.item',
  'PUT /request/:id/reject': {
    controller: 'Request',
    action: 'reject',
    cors: true
  },
  'PUT /request/:id': 'request.update',
  'DELETE /request/:id': 'request.delete',


  'GET /reception': 'reception.list',
  'POST /reception': 'reception.create',
  'GET /reception/new': 'reception.new',
  'GET /reception/:id': 'reception.item',
  'PUT /reception/:id': 'reception.update',
  'DELETE /reception/:id': 'reception.delete',
  'GET /reception/:id/edit': 'reception.edit',
  

  'GET /doctor': 'doctor.list',
  'POST /doctor': 'doctor.create',
  'GET /doctor/new': 'doctor.new',
  'GET /doctor/:id': 'doctor.item',
  'PUT /doctor/:id': 'doctor.update',
  'DELETE /doctor/:id': 'doctor.delete',
  'GET /doctor/:id/edit': 'doctor.edit',
  
  
  'GET /user': 'user.list',
  'POST /user': {
    controller: 'User',
    action: 'create',
    cors: false
  },
  'GET /user/jwt': 'user.jwt',
  'GET /user/:id': 'user.item',
  'PUT /user/:id': {
    controller: 'User',
    action: 'update',
    cors: false
  },
  // 'DELETE /user/:id': 'user.delete',
  
  // 'POST /comment': 'comment.create',
  // 'GET /comment/:id': 'comment.item',
  // 'PUT /comment/:id': 'comment.update',
  // 'DELETE /comment/:id': 'comment.delete',
  
  'GET /service': 'service.list',
  'POST /service': 'service.create',
  'GET /service/new': 'service.new',
  'GET /service/:id': 'service.item',
  'PUT /service/:id': 'service.update',
  'DELETE /service/:id': 'service.delete',
  'GET /service/:id/edit': 'service.edit',
  
  

};
