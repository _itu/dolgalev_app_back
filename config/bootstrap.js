var dDocrors = [
    {
      "last_name": "Долгалев",
      "first_name": "Александр",
      "second_name": "Александрович",
      "email": "enim.Etiam.gravida@mollis.edu",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Долгалева",
      "first_name": "Людмила",
      "second_name": "Анатольевна",
      "email": "gravida@mi.ca",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Соболев",
      "first_name": "Дмитрий",
      "second_name": "Александрович",
      "email": "ut@sociisnatoque.net",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Корниенко",
      "first_name": "Сергей",
      "second_name": "Владимирович",
      "email": "at.sem@tinciduntvehicula.ca",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Черная",
      "first_name": "Инна",
      "second_name": "Анатольевна",
      "email": "nibh.Donec.est@augueSedmolestie.org",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Миленина",
      "first_name": "Алла",
      "second_name": "Адамовна",
      "email": "vel.sapien.imperdiet@at.com",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "id": 7,
      "last_name": "Сегида",
      "first_name": "Ирина",
      "second_name": "Владимировна",
      "email": "sed@Maurisquis.edu",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Шагимарданова",
      "first_name": "Анна",
      "second_name": "Владиславовна",
      "email": "quis.diam.luctus@atfringillapurus.org",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    },
    {
      "last_name": "Соколовская",
      "first_name": "Марина",
      "second_name": "Руслановна",
      "email": "Integer.tincidunt@primis.org",
      "schedule": [{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'},{'begin': '8:00', 'end': '20:00'}]
    }
  ]

module.exports.bootstrap = function(cb) {

  var addFunc = [

      function (next) {

        Doctor.count().exec(function (err, count) {
          if (err) return next(err)
          if (count > 0) return next()
          
          Doctor.create(dDocrors).exec(next)
        })
      }

  ];


  async.series(addFunc,
    function (err, result) {
      if (err) {
        sails.log.error('cant push fake data');
        return cb(err)
      }

      cb();
    }
  )

};
