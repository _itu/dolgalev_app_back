module.exports.policies = {

  '*': 'isAdmin',

  ApiController: {
    '*': 'sessionAuth'
  },

  AuthController: {
    '*': true,
    'logout': 'sessionAuth',
    'jwt': 'hasJsonWebToken'
  },

  RequestController: {
    '*': 'isAdmin',
    'create': 'sessionAuth',
    'reject': 'sessionAuth',
  },
};
